<?php
/**
 * MultiFrete Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * Shipping MatrixRates
 *
 * @category   Magentonet
 * @package    Magentonet_MultiFrete
 * @copyright  http://www.magento.net.br
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Alex Braga
*/

class Magentonet_MultiFrete_Model_Carrier_MultiFrete
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'MultiFrete';
    protected $_default_condition_name = 'package_weight';

    protected $_conditionNames = array();

    public function __construct()
    {
        parent::__construct();
        foreach ($this->getCode('condition_name') as $k=>$v) {
            $this->_conditionNames[] = $k;
        }
    }

    /**
     * Enter description here...
     *
     * @param Mage_Shipping_Model_Rate_Request $data
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        //verifica se o modulo esta ativo no painel de controle
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        //fim da verifica��o se o modulo est� ativo


        $freeBoxes = 0;
        $found=false;
        $virtualTotal=0;
        foreach ($request->getAllItems() as $item) {
        	if ($item->getFreeShipping() && $item->getProductType()!= Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL
        		&& $item->getProductType() != 'downloadable') {
                    $freeBoxes+=$item->getQty();
            }
            if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL ||
                 $item->getProductType() == 'downloadable') {
                    $virtualTotal+= $item->getBaseRowTotal();
                    $found=true;
            }
        }
        if ($found && $this->getConfigFlag('remove_virtual')) {
        	$request->setPackageValue($request->getPackageValue()-$virtualTotal);
        }
        $this->setFreeBoxes($freeBoxes);

        //verifica qual � o tipo de frete ex: pesoxdestino e guarda na variavel $request
        $request->setConditionName($this->getConfigData('condition_name') ? $this->getConfigData('condition_name') : $this->_default_condition_name);
        //fim da verifica��o de tipo de frete

        //cria o objeto result que coloca o resultado do frete num array padr�o
        $result = Mage::getModel('shipping/rate_result');
        //fim da cria��o do objeto $result

     	$ratearray = $this->getRate($request);



     	//calcula se o frete � gratis acima de determinado valor
     	$freeShipping=false;

     	if ($this->getConfigData('enable_free_shipping_threshold') &&
	        $this->getConfigData('free_shipping_threshold')!='' &&
	        $this->getConfigData('free_shipping_threshold')>0 &&
	        $request->getPackageValue()>$this->getConfigData('free_shipping_threshold')) {
	         	$freeShipping=true;
	    }
    	if ($this->getConfigData('allow_free_shipping_promotions') &&
	        ($request->getFreeShipping() === true ||
	        $request->getPackageQty() == $this->getFreeBoxes()))
        {
         	$freeShipping=true;
        }
        if ($freeShipping)
        {
		  	$method = Mage::getModel('shipping/rate_result_method');
			$method->setCarrier('MultiFrete');
			$method->setCarrierTitle($this->getConfigData('title'));
			$method->setMethod('MultiFrete_free');
			$method->setPrice('0.00');
			$method->setMethodTitle($this->getConfigData('free_method_text'));
			$result->append($method);

			if ($this->getConfigData('show_only_free')) {
				return $result;
			}
		}
        //fim do frete gratis acima de determinado valor



      //aqui entra a parte do c�digo que controla o frete por produto
      if ($this->getConfigFlag('enable_per_product')) { //pergunta ao magento se no painel de controle esta ativado a flag "calcular por produto"
        $method = Mage::getModel('shipping/rate_result_method'); //inicializa o m�todo
		$method->setCarrier('MultiFrete');
		$method->setCarrierTitle($this->getConfigData('title'));
		$method->setMethod('MultiFrete_'.$rate['pk']);
		$method->setMethodTitle(Mage::helper('MultiFrete')->__($rate['empresa_tipo']));

         foreach ($request->getAllItems() as $item) { //passa por todos os itens do carrinho
               //aqui verifica se � um programa gratis ou virtual
               if (!$item->getFreeShipping() && $item->getProductType()!= Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL && $item->getProductType() != 'downloadable')
               {
			         	for($i=0;$i<$item->getQty();$i++) //verifica a quantidade de cada item
			         	{
			            		$request->setPackageWeight($item->getWeight()); //coloca o peso somente deste produto
			        		    $request->getPackageQty($item->getQty()/$item->getQty()); //coloca a quantidade somente deste produto
			        		    $request->setPackagePrice($item->getPrice()); //coloca o pre�o somente deste produto
			       		        $ratearray = $this->getRate($request);
			                    foreach ($ratearray as $rate) //enquanto tiver transportadora com as mesmas caracteristicas ele continua
	   							{
	   							    $transportadorafrete[$rate['empresa_tipo']]['preco'] += $this->getFinalPriceWithHandlingFee($rate['price']);
	   							    $transportadorafrete[$rate['empresa_tipo']]['empresa_tipo'] = $rate['empresa_tipo'];
	   							    $transportadorafrete[$rate['empresa_tipo']]['cost'] = $rate['cost'];
	   							    $transportadorafrete[$rate['empresa_tipo']]['pk'] = $rate['pk'];
	   							    $nometransportadora[$rate['empresa_tipo']] = $rate['empresa_tipo']; //conta quantas vezes essa transportadora foi usada
	   							    $contatransportadora[$rate['empresa_tipo']]++;
	   							}
	   							$contaprodutos++; //conta todos os produtos que ser�o enviados

			         	}
               }
         }

         foreach ($nometransportadora as $nome) //olha quais transportadoras foram chamadas - usei a $nome_transportadora pq � mais facil de maipular que a matriz
         {
                if($contatransportadora[$nome] <> $contaprodutos) { continue; }
                $method = Mage::getModel('shipping/rate_result_method'); //cria uma instancia da classe rate_result_method
                $method->setCarrier('MultiFrete'); //nome do carrinho
				$method->setCarrierTitle($this->getConfigData('title')); //titulo do carrinho
                //escreve novamente o m�todo e o titulo pego no foreach
                $method->setMethod('MultiFrete_'.$transportadorafrete[$nome]['pk']);
		        $method->setMethodTitle(Mage::helper('MultiFrete')->__($transportadorafrete[$nome]['empresa_tipo']));
				$method->setCost($transportadorafrete[$nome]['cost']);
				$method->setDeliveryType($transportadorafrete[$nome]['empresa_tipo']);
				$method->setPrice($transportadorafrete[$nome]['preco']);
				$result->append($method);
          }

       return $result;//finaliza a rotina para n�o calcular 2 vezes
       }
       //fim da parte do frete que controla por produto

	   foreach ($ratearray as $rate)
		{
		   if (!empty($rate) && $rate['price'] >= 0) {
			  $method = Mage::getModel('shipping/rate_result_method');

				$method->setCarrier('MultiFrete');
				$method->setCarrierTitle($this->getConfigData('title'));

				$method->setMethod('MultiFrete_'.$rate['pk']);

				$method->setMethodTitle(Mage::helper('MultiFrete')->__($rate['empresa_tipo']));

				$shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
				$method->setCost($rate['cost']);
				$method->setDeliveryType($rate['empresa_tipo']);

				$method->setPrice($shippingPrice);

				$result->append($method);
			}
		}



        return $result;
    }

    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('MultiFrete_shipping/carrier_MultiFrete')->getNewRate($request,$this->getConfigFlag('zip_range'));
    }

    public function getCode($type, $code='')
    {
        $codes = array(

            'condition_name'=>array(
                'package_weight' => Mage::helper('shipping')->__('Weight vs. Destination'),
                'package_value'  => Mage::helper('shipping')->__('Price vs. Destination'),
                'package_qty'    => Mage::helper('shipping')->__('# of Items vs. Destination'),
            ),

            'condition_name_short'=>array(
                'package_weight' => Mage::helper('shipping')->__('Weight (and above)'),
                'package_value'  => Mage::helper('shipping')->__('Order Subtotal (and above)'),
                'package_qty'    => Mage::helper('shipping')->__('# of Items (and above)'),
            ),

        );

        if (!isset($codes[$type])) {
            throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Matrix Rate code type: %s', $type));
        }

        if (''===$code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Matrix Rate code for type %s: %s', $type, $code));
        }

        return $codes[$type][$code];
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array('MultiFrete'=>$this->getConfigData('name'));
    }

}
