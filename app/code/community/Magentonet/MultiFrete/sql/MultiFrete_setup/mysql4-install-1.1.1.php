<?php

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('shipping_MultiFrete')} (
  pk int(10) unsigned NOT NULL auto_increment,
  website_id int(11) NOT NULL default '0',
  dest_pais_id varchar(4) NOT NULL default '0',
  dest_regiao_id int(10) NOT NULL default '0',
  dest_cidade varchar(30) NOT NULL default '',
  dest_CEP varchar(10) NOT NULL default '',
  dest_CEP_ate varchar(10) NOT NULL default '',
  condition_name varchar(20) NOT NULL default '',
  condicao_de decimal(12,4) NOT NULL default '0.0000',
  condicao_ate decimal(12,4) NOT NULL default '0.0000',
  price decimal(12,4) NOT NULL default '0.0000',
  cost decimal(12,4) NOT NULL default '0.0000',
  empresa_tipo varchar(255) NOT NULL default '',
  PRIMARY KEY(`pk`),
  UNIQUE KEY `dest_country` (`website_id`,`dest_pais_id`,`dest_regiao_id`,`dest_cidade`,`dest_CEP`,`dest_CEP_ate`,`condition_name`,`condicao_de`,`condicao_ate`,`empresa_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



   ");

$installer->endSetup();


